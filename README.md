Copyright (C) 2019  INRIA

This program is released under the GNU Affero General Public License Version 3.0-or-later.

# Treerecs
Treerecs is an open-source (species- and gene-) tree reconciliation software available for Linux, Windows and MacOS.
It can correct, rearrange and (re-)root gene trees with regard to a given species tree.

This README contains condensed information about how to install and how to use Treerecs.
For *detailed* information, please visit [the website](https://project.inria.fr/treerecs/).

# Installation

## Windows
Coming soon...

## Linux and MacOS

### Binaries
Coming soon...

### From source
#### 1. Get the source
##### Latest stable release:
* [download zip](https://gitlab.inria.fr/Phylophile/Treerecs/repository/archive.zip?ref=master)
* [downoad tar.gz](https://gitlab.inria.fr/Phylophile/Treerecs/repository/archive.tar.gz?ref=master)

##### Development version (unstable)
* [download zip](https://gitlab.inria.fr/Phylophile/Treerecs/repository/archive.zip?ref=dev)
* [downoad tar.gz](https://gitlab.inria.fr/Phylophile/Treerecs/repository/archive.tar.gz?ref=dev)

#### 2. Build

    cmake -DCMAKE_BUILD_TYPE=Release .
    make
    
The created executable can be found in the *bin* directory.

    ./bin/treerecs -h

##### Note for Mac users:
The `-P | --parallelize` option requires the compiler to support openmp, which may not be the case for the default compiler. Consider using another compiler instead.
    
#### 3. Install (optional)

    sudo make install
    
If you ever wish to uninstall Treerecs:

    sudo make uninstall

# Usage
## Syntax
*    `treerecs -h` or `--help`
*    `treerecs -V` or `--version`
*    `treerecs --usage`
*    `treerecs -g GENETREE_FILE -s SPECIESTREE_FILE [-S SMAP_FILE] [-t BRANCH_SUPPORT_THRESHOLD] [...]`
*    `treerecs -g GENETREE_FILE --info`

For option details, please visit [this page](https://project.inria.fr/treerecs/treerecs-options/).

## Tutorial
A tutorial is available at [https://project.inria.fr/treerecs/tutorial/](https://project.inria.fr/treerecs/tutorial/)

# References
* [__Efficient Gene Tree Correction Guided by Genome Evolution (2016)__,
    Emmanuel Noutahi,
    Magali Semeria, 
    Manuel Lafond, 
    Jonathan Seguin, 
    Bastien Boussau, 
    Laurent Guéguen, 
    Nadia El-Mabrouk,…](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0159559)
* [__An Optimal Reconciliation Algorithm for Gene Trees with Polytomies__,
    Manuel Lafond,
    Krister M. Swenson,
    Nadia El-Mabrouk](http://link.springer.com/chapter/10.1007/978-3-642-33122-0_9)
* [__Genome-scale phylogenetic analysis finds extensive gene transfer among fungi__,
    Gergely J. Szöllősi,
    Adrián Arellano Davín, 
    Eric Tannier, 
    Vincent Daubin, 
    Bastien Boussau](http://rstb.royalsocietypublishing.org/content/370/1678/20140335.long)

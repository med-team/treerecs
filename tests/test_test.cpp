// Copyright (C) 2018  INRIA
//
// Treerecs is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Treerecs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gtest/gtest.h"

#include <vector>
#include <treerecs/tools/utils.h>

class test_test : public testing::Test {
protected:
  virtual void SetUp() {}

  virtual void TearDown() {}

  std::vector<int> values = {-1, 1};
};

TEST_F(test_test, testTest) {
  ASSERT_EQ(0, treerecs::utils::sum(values));
}

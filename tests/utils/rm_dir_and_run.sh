#! /bin/bash

################################################################################
# Remove directory then run command
# $1 the output directory
# $2 the command to run
################################################################################

# Remove outdir
rm -rf $1

# Run command
$2

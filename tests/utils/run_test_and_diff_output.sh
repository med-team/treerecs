#! /bin/bash

################################################################################
# Run command then diff output_dir vs ref_out_dir
# $1 the command to run
# $2 the output directory
# $3 the reference output directory
################################################################################

# Reset outdir
rm -rf $2/*

# Run command
$1

fail=0
if ! diff $2 $3; then fail=1; fi

if test $fail -ne 0; then exit -1; fi

// Copyright (C) 2018  INRIA
//
// Treerecs is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Treerecs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gtest/gtest.h"

#include <string>

#include "examples_paths.h"
#include <treerecs/tools/SpeciesGeneMapper.h>
#include <treerecs/tools/IO/IO.h>
#include <treerecs/tools/IO/RecPhyloXML.h>
#include <treerecs/Constants.h>

class RecPhyloXML_tests: public testing::Test {
protected:
    std::string map_file = multipolytomy_examples_path
                           + "map.txt";
    std::string speciestree_file = multipolytomy_examples_path
                                   + "species_tree.txt";
    std::string genetree_file = multipolytomy_examples_path
                                + "gene_tree.txt";
};

TEST_F(RecPhyloXML_tests, testOutput) {
  auto genetree = IO::readTreeFile(genetree_file);
  auto speciestree = IO::readTreeFile(speciestree_file, false, false, false);
  auto map = SpeciesGeneMapper::mapFromFile(map_file, *speciestree, *genetree);
  SpeciesGeneMapper::updateInnerNodeMapping(map, *genetree, *speciestree);

  std::string rec_output_filename = "RecPhyloXML_filetest.xml";
  std::string speciestree_output_filename = "speciesTreePhyloXML_filetest.xml";
  std::string genetree_output_filename = "geneTreePhyloXML_filetest.xml";

  std::ofstream speciesxml(speciestree_output_filename);
  RecPhyloXML::speciesTreeToPhyloXML(speciesxml, *speciestree, 0);
  speciesxml.close();

  std::ofstream genexml(genetree_output_filename);
  RecPhyloXML::geneTreeToRecPhyloXML(genexml, *genetree, 1, 0);
  genexml.close();

  std::ofstream phyloxml(rec_output_filename);
  RecPhyloXML::reconciliationToRecPhyloXML(phyloxml, *speciestree, *genetree);
  phyloxml.close();

  RecPhyloXML::reconciliationToRecPhyloXML(std::cout, *speciestree, *genetree);
}

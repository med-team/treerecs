// Copyright (C) 2018  INRIA
//
// Treerecs is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Treerecs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gtest/gtest.h"

#include <string>

#include "examples_paths.h"
#include <treerecs/tools/LibpllEvaluation.h>

using namespace treerecs;
/*!
 * @class treeReconciliation_test
 * @brief Test TreeReconciliationConductor.
 */
class LibpllEvaluation_tests  : public testing::Test {
protected:

  //std::string alignment_file = examples_path
  // + "libpll_examples/real_example/HBG011000.fasta";
  //std::string genetree_file = examples_path
  // + "libpll_examples/real_example/gene_tree_HBG011000.newick";
  std::string alignment_file = alignment_examples_path + "alignment_example.phy";
  std::string genetree_file = alignment_examples_path + "alignment_example_tree.txt";
};

TEST_F(LibpllEvaluation_tests, testEvaluation) { 
  LibpllAlignmentInfo info;
  info.alignmentFilename = alignment_file;
  info.model = "GTR";
  auto evaluation = LibpllEvaluation::buildFromFile(genetree_file, info);
  std::cout << "> Computed likelihood = "
            << evaluation->computeLikelihood() << std::endl;
  std::cout << "> Optimized likelihood = "
            << evaluation->optimizeAllParameters() << std::endl;

};

treerecs -g genetree.nwk -s speciestree.nwk -O newick:svg -t 98 -o out_t98
treerecs -g genetree.nwk -s speciestree.nwk -O newick:svg -t 98+epsilon -o out_t98+e
